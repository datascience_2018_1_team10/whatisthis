package com.korea.android.whatisthis;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.util.Locale;

public class PreviewActivity extends Activity {
    private TextToSpeech tts;
    private String koreanLabel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);

        String filePath = getIntent().getStringExtra("imgFilePath");

        Glide.with(this).load(filePath)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into((ImageView) findViewById(R.id.imageview));

        tts = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status != TextToSpeech.ERROR) {
                    tts.setPitch(2.0f);         // 음성 톤을 2.0배 올려준다.
                    tts.setSpeechRate(1.0f);    // 읽는 속도는 기본 설정
                }
            }
        });

        String label = getIntent().getStringExtra("label");

        String JSON_DATA = "";
        try {
            JSON_DATA = Util.getTranslatedLabelJson(this);
            Log.d("PrintJson", JSON_DATA);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (!TextUtils.isEmpty(JSON_DATA)) {
            JsonObject root = new JsonParser().parse(JSON_DATA).getAsJsonObject();
            JsonArray subnode = root.get(label).getAsJsonArray();
            koreanLabel = subnode.get(0).getAsString();
            Log.d(label, koreanLabel);
        }

        View playInEnglish = findViewById(R.id.play_in_english);
        playInEnglish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tts.setLanguage(Locale.ENGLISH);
                tts.speak(label, TextToSpeech.QUEUE_FLUSH, null);
            }
        });

        View playInKorean = findViewById(R.id.play_in_korean);
        playInKorean.setVisibility(TextUtils.isEmpty(koreanLabel) ? View.GONE : View.VISIBLE);
        playInKorean.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tts.setLanguage(Locale.KOREAN);
                tts.speak(koreanLabel, TextToSpeech.QUEUE_FLUSH, null);
            }
        });

        View searchToInternet = findViewById(R.id.icon_search_internet);
        searchToInternet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = !TextUtils.isEmpty(koreanLabel) ? koreanLabel : label;
                String query = text.replaceAll(" ", "+");
                String url = "https://terms.naver.com/search.nhn?query=" + query + "&searchType=text&dicType=&subject=";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
            }
        });

        ((TextView) findViewById(R.id.label)).setText(koreanLabel + "\n" + label);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // TTS 객체가 남아있다면 실행을 중지하고 메모리에서 제거한다.
        if (tts != null) {
            tts.stop();
            tts.shutdown();
            tts = null;
        }
    }
}

