package com.korea.android.whatisthis;

import android.app.Activity;
import android.graphics.Bitmap;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class Util {
    private static final String _TAG = Util.class.getSimpleName();

    public static String saveFile(String path, String fileName, Bitmap target) {
        File file = new File(path);
        file.setReadable(true, false);

        if (!file.exists()) {
            if (file.mkdirs()) {
                Log.d(_TAG, "Directory Created");
            } else {
                Log.e(_TAG, "Fail Directory Create");
            }
        }

        String fileExtension = fileName.substring(fileName.lastIndexOf(".") + 1);
        String filePath = file.getPath() + "/" + fileName;

        try {
            FileOutputStream fileOutput = new FileOutputStream(filePath);
            BufferedOutputStream bufferOutput = new BufferedOutputStream(fileOutput);

            if (fileExtension.equalsIgnoreCase("png")) {
                target.compress(Bitmap.CompressFormat.PNG, 0, bufferOutput);
            } else if (fileExtension.equalsIgnoreCase("jpg")) {
                target.compress(Bitmap.CompressFormat.JPEG, 100, bufferOutput);
            }

            bufferOutput.flush();
            return filePath;

        } catch (FileNotFoundException ex) {
            Log.e(_TAG, "file not found exception for saving file");
        } catch (IOException e) {
            // include out of memory exception
            Log.e(_TAG, e.getMessage(), e);
        } catch (Exception e) {
            Log.e(_TAG, "save exception ::: " + e);
        }
        return null;
    }

    /** Reads translated label from Assets. */
    public static String getTranslatedLabelJson(Activity activity) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader reader =
                new BufferedReader(new InputStreamReader(activity.getAssets().open("translated_label.json")));
        String line;
        while ((line = reader.readLine()) != null) {
            sb.append(line);
        }
        reader.close();
        return sb.toString();
    }
}
